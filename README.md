# WEB 200 Maryann Jordan

mareann99.bitbucket.io

## Assignment 1 

I am working on my Web Design/Interact Media Certificate in order to improve my knowledge of web development.
Taking the Web Design classes helps me understand the process and allows me to explore my skills.
I am looking forward to refreshing my knowledge of Javascript in the Web Foundations class.
I have previous Javascript experience from Harper continuing education classes.

## Assignment 2 Variables

    I learned that variable names are written in camelCase and constant names are written in UPPER_CASE. Noting that variable names are case sensitive (easy mistake).
    There are 7 data types: Number, String, Boolean, Null, Undefined, Function, and Object. Null is an empty value. Undefined variable is defined but not assigned a value.

    The typeof function returns the data type of a variable (out of the 7 data types)
    Strings can be defined using single or double quotes. I try to use double quotes for more than one character ("words") and single quote for one character (like 'a').

    There are builtin functions to convert data types.
    A number can be used with or without a decimal point. 

Activity 1: 
    Creating an Age Calculator program uses the input prompt function, const and let data type, and document.getElementById to modify the display.

Activity 2:
    Creating a program in the "Data Types" section that initializes different variables with integer, floating point, string, null, and undefined values. 
    Demo various operations and converting between data types. This is great practice to understand the usage of these different data types.
	Most programs will utilize data types to display its data.
    
## Assignment 3 Functions

    We are learning about the use of functions to make your code more readable, managable and efficient.
    Functions are a block of code that can accept input parameters to compute and return a value.
    The function can return any data type, for example, it could return a boolean, string, object or number data type.
    The function can be called many times in your code to minimize lines of code so it is not necessary to repeat the same code over and over.

Activity 1:
     Create a program that uses variables for years, 
     and then calculate and displays an approximate age
     in months, days, hours, and seconds. For example, 
     a person 1-year-old is 12 months old, 365 days old, etc.
     
Activity 2:
    Prompt the user how old they are in years
    - Display approximate age in months, days, hours, and seconds
     For example, a person 1 year old is 12 months old, 365 days old, etc.
     
Activity 3:
    Create a program that asks the user for a distance in miles
    calculate and display the distance in yards, feet, and inches

## Assignment 4 Events

    We are learning about event handlers. These can be a difficult subject so I recommend just keep looking at examples.
    I found this resource for good examples:
        https://www.w3schools.com/tags/ref_eventattributes.asp
    There are different kinds of event handlers.
    Window events triggers for the window object (body tag). For example onload occurs after all the code is loaded for the page.
    Form events are events triggered by actions inside a html form or other html elements.
    The form event onchange is triggered when the value is of an element is changed. 
    So when the user starts typing in an input field this event will be fired up. So it could call
    an function that verifies the input. Is the input a valid email address etc?

    Keyboard events happen when a key is pressed, onkeydown is an example. The onkeydown is 
    an event that fires while the user is pressing a key down to use it. So after pressing down on a key there would be a onkeyup 
    (on its way back to resting position). There is also an onkeypress event after the key has returned to its resting position.


    One of the most used event handlers are mouse events. Events like onmouseover, onmouseout, or onclick are examples of a mouse event. 
    The onmouseover and onmouseout are fired when the mouse pointer are moved over or away from the element, respectively. 
    The onclick is when the element is selected by the mouse. I would think the enter key would fire the onclick event also.
    
    Activity 1: Pay Calculator
    Activity 2: Age in months,days,hours,seconds
    
## Assignment 5 Conditionals

    This week we are learning about conditional statements, switch statements and valuate expressions.
    Some logical and comparison operators used are ==; !=; <, >; <=; >=; !; &&; ||. 

    The conditional "if" is used for a simple comparisons. For example, a "if" true condition would execute its code block,
    if not true (false) "else" would execute its code block. An "else if" would be used to test for an additional condition.

    The switch is used for complicated condition with many possible choices. The switch expression value is calculated and 
    then compared with each case value in order. When a case value matches the switch value, its code block is executed. 
    Then it continues execution until there is a break or the end of the switch. If none of the case values match the switch value, 
    it would run the default code block or just exit the switch. 

    One thing I learned about switch statements is the switch expression is compared to case values using strict comparison (===). 
    This means the values must be the same types to match. So the value of 2 is not equal to "2".

    Also the switch can not be broken out of by using a continue keyword.
    
    Activity 1: Pay Calculator
    Activity 2: Age in months,days,hours,seconds
    
## Assignment 6 While loops

    We are learning about loops. While, do while and for loops are commonly used to run the same code with 
    one or more variables changing its value. These help your code to be DRY (do not repeat). 

    The for loop variable usually is incremented by 1 each time it executes its code with the current variable value.
    This allows the loop to run a specific number of times.

    The while statement keeps executing as long as its expression is true. To avoid the dreaded infinite loop
    an end condition must be provided. For example if expression is a < 3, then there should be incrementing
    code for a to insure the value 3 happens.

    The do while is similar to the while but it always runs its code at least once. This is because the
    condition is at the bottom of the loop. If the condition is not met, if does not return to the top of
    its loop. These are not as common due to the fact that it always runs once. This can be prone to some 
    error issues.
    
    Activity 1: Average of Grades
    Activity 2: Guessing Game
    
## Assignment 7 For loop

    This week we are learning about for loops. For loops are great for running the same code block with different value of a variable. 

    The for loop can be used to calculate averages or summary values.

    The for loop has 3 parts:

    variable starting value
    condition expression - variable is greater than, greater or equal, less than, less than or equal, or equal to a specified value
    variable value change - increment or decrement

    The for loop
     - runs the code block with the initial starting value,
     - updates the variable value in the 3rd part,
     - checks the condition expression, if true the code block is executed, if false it exits the loop

    The for loop runs a code block a specified number of times. This is a reliable way to run a code block more than once. 

    Activity 1: Average of Grades
    Activity 2: Multiple Expressions
    
## Assignment 8 Arrays

    Arrays can store more than one data value. The data is accessed by using an index number inside brackets "[ ]"

    The first number in the array is accessed using the index of 0. For examle, arrayName[0] is the first value. The index
    is then increased by 1 for each data value added.

    Arrays can be multidimentional. Multidimentional arrays are arrays of arrays.

    Arrays are a great way to organize data.
    
    Activity 1: Average of Grades
    Activity 2: Multiple Expressions

## Assignment 9 Dates

    We learned about the Date object. The object date and time parts can be retrieved separately with the date get methods. This way the date can be display in the preferred format.  If you wanted just the month and day of month, you would use getMonths and getDate methods. 

    To keep the date time display up to date, there are timing events like setTimeout() and setInterval().

    There are set methods but those are not as commonly used. 
    
    Activity 1: Current Date Display
    Activity 2: Date Picker
    
## Assignment 10 Objeccts

    Objects are the subject of the this weeks lesson. Objects are a variable that contains a set of of related values. 
    For example, a recipe ingredients and the instructions can be stored in an object. The object can store
    2 kinds of variables, static properties and dynamic methods.
    
    Activity 1: Book List
    Activity 2: Book List with Method
    
## Assignment 11 DOM and BOM

    In this session, we learned about HTML Document Object Model (DOM) and BOM. DOM is a standard object model. 
    Browser Object Model (BOM) is a browser-specific convention. 
    
    DOM defines html elements as objects. DOM allows access to properties, methods and events for all html elements. 
    DOM can get, change, add, or delete html elements.

    BOM allows javascript to access the browser. The BOM can access the history, screen size, location and cookies for the brower.
    
    Activity 1: Book List
    Activity 2: Book List with Method

    Interactivity with the user is a great feature of Javascript!
    
## Assignment 12 Dynamic HTML

    This week we learn how to dynamically change your HTML with JavaScript.

    First you need to design the layout of the section and then create each node as needed.

    It helps you understand the structure of the webpage and learn how to update as required.

    Now we are all be familiar with getElementByld; getElementsByTagName; getElementsByClassName; setAttribute; and createElement.

    Since this is a simple adding to a list, I used getElementById but there are many senarios where the others would be better to use.

    For the activities I also utilized createElement, createTextNode, and appendChild. It was good to learn these for future projects.
    
    Activity 1: Book List
    Activity 2: Book List with Method

## Assignment 13 Forms and Security
    
    Forms are very important to keep in contact with your customers or neighbors. We learned how to validate the form using its attributes. 

    There are many attributes to choose from like min,max,size,pattern, minlength.

    Input tag type field can be used for verifying. Type field includes numbers,text,email,tel,date and others.

    Then javascript validation api, checkValidity() can be used to validate the date. Error message can be displayed to assist the customers with their entry data.
    
## Assignment 14 AJAX and JSON

    Section 14 is about AJAX and JSON.

    We learned how to generate JSON data format. We posted the form data using ajax post. We retrieved data using

    the ajax get feature.

    We simulated an API using the https://jsonplaceholder.typicode.com API simulator. We performed the get and post functions using this API simulator.

    This is has been the most challenging week for me. All of the different options overwhelmed me for a while.

## Final Project Forms, Objects, AJAX, JSON

    We created a pizza app for our final project. We use an Order object, which contains a Customer Object, and array of Pizza Objects.

    The app has a total price displayed for the pizza including its toppings. After a pizza is added, a subtotal, tax and total are display for the customer.

    The customer can add as many pizzas as they can afford. Then place the order using the Submit Order button. The data is posted in JSON data with AJAX calls.

    The customer personal information should be entered and verified before submitting order.

    