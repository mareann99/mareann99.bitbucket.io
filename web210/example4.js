// Maryann Jordan Web 210
// Demostrates the principle 2.1.1 Keyboard: The user can navigate using only the keyboard. 
// This is necessary for many of the disabled users who can not use a mouse or possible even use of their arms.
//
// Demostrates the Guideline 3.1 Readable: Make text content readable and understandable. 
// This guideline makes sure the text is easily read and understood by all users with or without their screen reader. 
// This example tested well with NVDA screen reader. After the user selects the calculate button, 
// the screen reader makes complete sentences. I use the word dollars after the value to help the screen reader.
// The input fields and the button were labeled with clear descriptions.

// Calculate weekly, monthly, and annual gross pay
// using inputted hours worked per week and rate per hour
const WEEKS_IN_YEAR=52;
const MONTHS_IN_YEAR=12;

"use strict";

// clear values
document.getElementById("inputPay").value="";
document.getElementById("inputHours").value="";

// wait for click on button
document.getElementById("myBtn").addEventListener("click", main);


function inputPayFn() {
    let hourlyPay = document.getElementById("inputPay").value;
    console.log("input pay "+hourlyPay);
    return(hourlyPay);
}

function inputHoursFn() {
    let hours = document.getElementById("inputHours").value;
    console.log("input hours "+hours);
    return(hours);
}


// main function initiates other functions to calculate and display results to webpage
function main() {
    let pay = inputPayFn();
    let hours = inputHoursFn(); 
    let weeklyPay = getWeeklyPay(hours,pay);  
    let monthlyPay = getMonthlyPay(hours,pay);
    let annualPay = getAnnualPay(hours,pay);
    console.log("main weeklyPay "+weeklyPay);
    // Display results to webpage
    document.getElementById("pay").innerHTML=pay;
    document.getElementById("hours").innerHTML=hours;
    displayResults(weeklyPay,monthlyPay,annualPay);
    // clear values

document.getElementById("inputPay").value="";
document.getElementById("inputHours").value="";
}


// Calculate weekly pay using hours and pay rate
function getWeeklyPay(hours,pay) {
    let weeklyPay = hours * pay;
    console.log("getweek: hours "+hours);
    console.log("getweek: weeklypay "+weeklyPay);
    return(weeklyPay);
}

// Calculate monthly pay using hours and pay rate
function getMonthlyPay(hours,pay) {
    let monthlyPay = ((hours * pay) *  WEEKS_IN_YEAR) / MONTHS_IN_YEAR;
    console.log("monthlypay "+monthlyPay);
    return(monthlyPay);
}

// Calculate annual pay using hours and pay rate
function getAnnualPay(hours,pay) {
    let annualPay = (hours * pay) * WEEKS_IN_YEAR;
    console.log("annualPay "+annualPay);
    return(annualPay);
}

// Display pay results on screen
function displayResults(weeklyPay,monthlyPay,annualPay) {
  document.getElementById("weeklyPay").innerHTML=weeklyPay.toFixed(2);
  document.getElementById("monthlyPay").innerHTML=monthlyPay.toFixed(2);
  document.getElementById("annualPay").innerHTML=annualPay.toFixed(2);
  console.log("display annualPay "+annualPay);
}









