// Maryann Jordan Web 200
// Display information 
// Assignment 14 - AJAX
// Use an online API simulation tool such as jsonplaceholder.typicode.com to test your code. 
// Depending on configuration of your API simulation tool, the returned data may or may not 
// match the data you send. The intent is to test XMLHttpRequest communication rather than 
// the API itself.
// Send form data using AJAX and JSON rather than through form submission. 
// Use POST to add users and use GET to retrieve user data.

"use strict";

// ref: https://en.wikiversity.org/wiki/JavaScript_Programming/AJAX_and_JSON/Example_Code
//testing data
function User(
    name=null, 
    username=null, 
    email=null, 
    address=null, 
    phone=null, 
    website=null,
    company=null) {
    this.name = name;
    this.username = username;
    this.email = email;
    this.address = address;
    this.phone = phone;
    this.website = website;
    this.company = company;
  }
  
  function Address(street=null, suite=null, city=null, zipcode=null, geolocation=null) {
    this.street = street;
    this.suite = suite;
    this.city = city;
    this.zipcode = zipcode;
    this.geo = geolocation;
  }
  
  function Geolocation(latitude=null, longitude=null) {
    this.lat = latitude;
    this.lng = longitude;
  }
  
  function Company(name=null, catchPhrase=null, bs=null) {
    this.name = name;
    this.catchPhrase = catchPhrase;
    this.bs = bs;
  }
  
window.addEventListener("load", function () {
  
    document.getElementById("get").addEventListener("click", getClick);
    document.getElementById("post").addEventListener("click", postClick);
    document.getElementById("get").focus();
  });


  function createUser() {
    let user = new User(
      "John Smith",
      "jsmith", 
      "jsmith@example.com",
      null, 
      "123-555-1234");
  
    user.address = new Address(
      "123 Any Street", 
      "Suite 1", 
      "Anytown", 
      "12345");
  
    user.address.geo = new Geolocation(
      -40.7831, 
      73.9712);
  
    user.company = new Company(
      "Dewey, Cheatem & Howe",
      "Your loss is our gain",
      "law office")
  
    return user;
  }

// testing section get data function
function getClick() {
    document.getElementById("url").innerText = 
    "https://jsonplaceholder.typicode.com/users/1?Leanne Graham"; // works!
    document.getElementById("data").innerText = "";
    document.getElementById("response").innerText = "";
    console.log("getFocus");
    let url = document.getElementById("url").innerText;
    let request = new XMLHttpRequest();
    request.open("GET", url);
    request.onload = function() {
      document.getElementById("response").innerText = "status: " + request.status + "\n";
      document.getElementById("response").innerText += "responseText:\n" + request.responseText;
      console.log("getClick() response ",document.getElementById("response").innerText);
      document.getElementById("data").innerText = request.responseText; // put in data section
    }
    request.send(null);
}

// testing section post data function
function postClick() {
    document.getElementById("url").innerText = 
    "https://jsonplaceholder.typicode.com/users";

    let user = createUser();
    document.getElementById("data").innerText = JSON.stringify(user, null, 2);
    document.getElementById("response").innerText = "";
    console.log("postFocus");
    let url = document.getElementById("url").innerText;
    let data = document.getElementById("data").innerText;
    let request = new XMLHttpRequest();
    request.open("POST", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = function() {
      document.getElementById("response").innerText = "status: " + request.status + "\n";
      document.getElementById("response").innerText += "responseText:\n" + request.responseText;
      console.log("postClick() response ",document.getElementById("response").innerText);
    }
    request.send(data);
}

document.getElementById("url").innerText = "";
document.getElementById("data").innerText = "";
document.getElementById("response").innerText = "";

// Last  First Address City  State Postal Email Phone Date of Birth
// object for Customer information
function CustomerListing(
    first,
    last,
    address,
    city,
    state,
    postal,
    country,
    email,
    phone,
    dob) {
        this.firstName = first;
        this.lastName = last;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postalCode = postal;
        this.country = country;
        this.emailAddress = email;
        this.phoneNUmber = phone;
        this.dateOfBirth = dob;
    }

// array of CustomerListing objects with Customer information
var CustomerList = [];

clearInputFields();

createData(); 

// create test data
function createData() {

    // create initial data 
    var c = new CustomerListing();

    // fill object with input information
    c.lastName     = "Smith";
    c.firstName    = "Mike";
    c.address      = "123 Main";
    c.postalCode   = "54321";
    CustomerList.push(c);
    if (CustomerList.length)
       console.log("create data",CustomerList[CustomerList.length-1]);
    else
       console.log("create error");
  }

// wait for click on add Customer button
document.getElementById("mySubmit").addEventListener("click", main);
// wait for click on add getAuthor data button
document.getElementById("getData").addEventListener("click", getData);

// The GET is used to retrieve data from the server. This is a read-only function
function getData()
{
  document.getElementById("url").innerText = "";
  document.getElementById("data").innerText = "";
  document.getElementById("response").innerText = "";

  if (CustomerList.length) // if there is a customer, retrieve last customer
    {
      document.getElementById("data").innerText = JSON.stringify(CustomerList[CustomerList.length-1]); 
      console.log("get number in list ",CustomerList.length); 
    }

  // get click
  let url = "https://mareann99.bitbucket.io/web200/assignment14/activity2.html/?Mike?Smith"
  document.getElementById("url").innerText = url;

  let request = new XMLHttpRequest();
  request.open("GET", url);
  request.setRequestHeader("Content-Type", "application/json");
  request.onload = function() {
    document.getElementById("response").innerText = "status: " + request.status + "\n";
    //document.getElementById("response").innerText += "responseText:\n" + request.responseText;
    console.log("getData get() response status ",request.status); //,document.getElementById("response").innerText);
    if (request.status == 200 ) {
        let data = document.getElementById("data").innerText;
        alert("Retrieved "+document.getElementById("data").innerText);
        console.log("getData() data ",data);
    }
  }
  request.send(null);
}

//The POST method sends data to the server and creates a new resource.
function postData() {
  // initialize html fields
  document.getElementById("url").innerText = "";
  document.getElementById("data").innerText = "";
  document.getElementById("response").innerText = "";
  if (CustomerList.length) // if there is a customer, retrieve last customer
  { 
    document.getElementById("data").innerText = JSON.stringify(CustomerList[CustomerList.length-1]); 
    console.log("post number in list ",CustomerList.length);
  }
 
  // Create a new object for storing the data.
  // Convert the new object to a string using your JSON parser.
  // Send the JSON string back to the client as the body.
  let data = document.getElementById("data").innerText;

  document.getElementById("response").innerText = "";

  let url = "https://mareann99.bitbucket.io/web200/assignment14/activity2.html";
  document.getElementById("url").innerText = url;

  let request = new XMLHttpRequest();
  request.open("POST", url, true);
  request.setRequestHeader("Content-Type", "application/json");
  
  // readystate codes
  //   0 (uninitialized) or (request not initialized)
  //   1 (loading) or (server connection established)
  //   2 (loaded) or (request received)
  //   3 (interactive) or (processing request)
  //   4 (complete) or (request finished and response is ready)

  request.onreadystatechange = function() {
    document.getElementById("response").innerText = "status: " + request.status + "\n";
    // document.getElementById("response").innerText += "responseText:\n" + request.responseText;
    console.log("postData() response ",request.status);
  }
  console.log("postData() data ",data);
  // Send JSON Data from the Server Side
  request.send(data);
}

/////////////////////////////////////////////////
//  field values
//  1     2       3     4     5      6      7      8       9    10
// Last  First Address City  State Postal Country Email Phone Date of Birth
// inputInfo
////////////////////////////////////////////////
function inputInfo(field) {
    var ret = 0;

    switch(field){
        case 1: 
            ret = document.getElementById("inputLastName").value;
            break;
        case 2: 
            ret = document.getElementById("inputFirstName").value;
            break;
        case 3: 
            ret = document.getElementById("inputAddress").value;
            break;
        case 4: 
            ret = document.getElementById("inputCity").value;
            break;
        case 5:
            ret = document.getElementById("inputState").value;
            break;
        case 6:
            ret = document.getElementById("inputPostal").value;
            break;
        case 7:
            ret = document.getElementById("inputCountry").value;
            break;
        case 8:
            ret = document.getElementById("inputEmail").value;
            break;
        case 9:
            ret = document.getElementById("inputPhone").value;
            break;
        case 10:
            ret = document.getElementById("inputDOB").value;
            break;
        default: console.log("inputInfo invalid value ",field);
            break;
   }

   return(ret);
}

function clearInputFields() {
    // clear values
    document.getElementById("inputLastName").value="";
    document.getElementById("inputFirstName").value="";
    document.getElementById("inputAddress").value="";
    document.getElementById("inputCity").value="";
    document.getElementById("inputState").value="";
    document.getElementById("inputPostal").value="";
    document.getElementById("inputCountry").value="";
    document.getElementById("inputEmail").value="";
    document.getElementById("inputPhone").value="";
    document.getElementById("inputDOB").value="";
}

// main function gathers input information to display results to webpage
function main() {

    var c = new CustomerListing();

    // fill object with input information
    c.lastName     = inputInfo(1);
    c.firstName    = inputInfo(2);
    c.address      = inputInfo(3);
    c.city         = inputInfo(4);
    c.state        = inputInfo(5);
    c.postalCode   = inputInfo(6);
    c.country      = inputInfo(7);
    c.emailAddress = inputInfo(8);
    c.phoneNumber  = inputInfo(9);
    c.dateOfBirth  = inputInfo(10);

    var valid = true;
    var inpObj = document.getElementById("inputLastName");
    if (!inpObj.checkValidity()) {
      valid = false;
      document.getElementById("errorMsg").innerHTML = "Last Name " + inpObj.validationMessage;
    }
    inpObj = document.getElementById("inputFirstName");
    if (!inpObj.checkValidity()) {
      valid = false;
      document.getElementById("errorMsg").innerHTML = "First Name " + inpObj.validationMessage;
    }
    inpObj = document.getElementById("inputAddress");
    if (!inpObj.checkValidity()) {
      valid = false;
      document.getElementById("errorMsg").innerHTML = "Address " + inpObj.validationMessage;
    }
    inpObj = document.getElementById("inputCity");
    if (!inpObj.checkValidity()) {
        valid = false;
        document.getElementById("errorMsg").innerHTML = "City " + inpObj.validationMessage;
    } 
    inpObj = document.getElementById("inputState");
    if (!inpObj.checkValidity()) {
        valid = false;
        document.getElementById("errorMsg").innerHTML = "State Abbreviation is two letters. " + inpObj.validationMessage;
    }
    inpObj = document.getElementById("inputPostal");
    if (!inpObj.checkValidity()) {
      valid = false;
      document.getElementById("errorMsg").innerHTML = "Postal Code is a 5 digit number. " + inpObj.validationMessage;
    } 
    inpObj = document.getElementById("inputCountry");
    if (!inpObj.checkValidity()) {
      valid = false;
      document.getElementById("errorMsg").innerHTML = "Country is a three letter code. " + inpObj.validationMessage;
    } 
    inpObj = document.getElementById("inputEmail");
    if (!inpObj.checkValidity()) {
      valid = false;
      document.getElementById("errorMsg").innerHTML = "Email " + inpObj.validationMessage;
    } 
    inpObj = document.getElementById("inputPhone");
    if (!inpObj.checkValidity()) {
      valid = false;
      document.getElementById("errorMsg").innerHTML = "Phone format is 123-456-7890. " + inpObj.validationMessage;
    } 

    if ( valid === true )
    {
        document.getElementById("mySubmit").disabled = false;
        document.getElementById("errorMsg").innerHTML = "";

        // Last  First Address City  State Postal Country Email Phone Date of Birth
        document.getElementById("LastName").innerHTML = "You added " + c.lastName;
        
        if (c.firstName)
         document.getElementById("FirstName").innerHTML = ", " + c.firstName;

        if (c.address)
          document.getElementById("Address").innerHTML = ", " + c.address; 
        
        if (c.city)
            document.getElementById("City").innerHTML =  c.city;
        else
            document.getElementById("City").innerHTML =  "";
       
        if (c.state)
            document.getElementById("State").innerHTML = ", " + c.state;
        else
            document.getElementById("State").innerHTML = " ";
        
        if (c.postalCode)
            document.getElementById("PostalCode").innerHTML = ", " + c.postalCode;
        else
            document.getElementById("PostalCode").innerHTML = " ";

        if (c.emailAddress)
            document.getElementById("Email").innerHTML = " " + c.emailAddress;
        else
            document.getElementById("Email").innerHTML = " ";

        if (c.country)
            document.getElementById("Email").innerHTML = " " + c.country;
        else
            document.getElementById("Email").innerHTML = "USA";

        if (c.phoneNumber)
            document.getElementById("Phone").innerHTML = " " + c.phoneNumber;
        else
            document.getElementById("Phone").innerHTML = " ";

        if (c.dateOfBirth)
            document.getElementById("DateOfBirth").innerHTML = " " + c.dateOfBirth ;
        else
            document.getElementById("DateOfBirth").innerHTML = " ";

        CustomerList.push(c);

     }
    else
     {
        document.getElementById("errorMsg").innerHTML = "<strong>Please add valid Customer information before selecting add button</strong>";  
        return;
     }

    // Display results to webpage
    // Last  First Address City  State Postal Country Email Phone Date of Birth
    // compose Customer info string for screen display
    var myCustomer = c.lastName;
    if (c.firstName)
        myCustomer += ", " + c.firstName;
    if (c.address)
        myCustomer +=  ", " + c.address;
    if (c.city)
        myCustomer +=  ", " + c.city;
    if (c.state)
        myCustomer += ", " + c.state;
    if (c.postalCode)
        myCustomer +=  ", " + c.postalCode;
    if (c.country)
        myCustomer +=  ", " + c.country;
    if (c.emailAddress)
        myCustomer +=  ", " +   c.emailAddress;
    if (c.phoneNumber)
        myCustomer +=  ", " + c.phoneNumber;   
    if (c.dateOfBirth)
        myCustomer +=  ", " + c.dateOfBirth;

    let newLi = document.createElement('li');  // create new list item
    var node = document.createTextNode(myCustomer); // create text of Customer
    newLi.appendChild(node); // append text to list item
    var elementList = document.getElementById("dynamicCustomerList");
    elementList.appendChild(newLi); // append list item to dynamicCustomerList
    if (CustomerList.length == 1) // create Customer List Header
    {
        let newHeader3 = document.createElement("h3");
        var nodeHeader3 = document.createTextNode("Customer List");
        newHeader3.appendChild(nodeHeader3);
        elementList.insertBefore(newHeader3, elementList.childNodes[0]);
    }

    var myJSON = JSON.stringify(c, null, 2);
    console.log("posting ",myJSON);
    postData(); //send data using ajax

    // clear values
    clearInputFields();
}
