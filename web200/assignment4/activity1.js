// Maryann Jordan Web 200 Assignment 2 Activity 1
// Calculate weekly, monthly, and annual gross pay
// using inputted hours worked per week and rate per hour
const WEEKS_IN_YEAR=52;
const MONTHS_IN_YEAR=12;

"use strict";

// prompt user for hours worked per week and store in hoursPerWeek var
//// let hoursPerWeek=window.prompt("Enter hours worked per week:");
//// document.getElementById("hours").innerHTML=hoursPerWeek;

// prompt user for hourly pay rate worked and store in hourlyPay var
////let hourlyPay=window.prompt("Enter pay per hour:");
////document.getElementById("pay").innerHTML=hourlyPay;
// window.addEventListener("load", function () {
//     document.getElementById("inputPay").addEventListener("input",  main);
//   });
document.getElementById("myBtn").addEventListener("click", main);
//option 1 main();

function inputPayFn() {
    let hourlyPay = document.getElementById("inputPay").value;
//    document.getElementById("pay").innerHTML=hourlyPay;
    console.log("input pay "+hourlyPay);
    return(hourlyPay);
}

function inputHoursFn() {
    let hours = document.getElementById("inputHours").value;
//    document.getElementById("hours").innerHTML=hours;
    console.log("input hours "+hours);
    return(hours);
}


// main function initiates other functions to calculate and display results to webpage
function main() {
    let pay = inputPayFn();
    let hours = inputHoursFn(); 
    let weeklyPay = getWeeklyPay(hours,pay);  
    let monthlyPay = getMonthlyPay(hours,pay);
    let annualPay = getAnnualPay(hours,pay);
    console.log("weeklyPay "+weeklyPay);
    // Display results to webpage
    document.getElementById("pay").innerHTML=pay;
    document.getElementById("hours").innerHTML=hours;
    displayResults(weeklyPay,monthlyPay,annualPay);
}


// Calculate weekly pay using hours and pay rate
function getWeeklyPay(hours,pay) {
    let weeklyPay = hours * pay;
    console.log("getweek: hours "+hours);
    console.log("getweek: weeklypay "+weeklyPay);
    return(weeklyPay);
}

// Calculate monthly pay using hours and pay rate
function getMonthlyPay(hours,pay) {
    let monthlyPay = ((hours * pay) *  WEEKS_IN_YEAR) / MONTHS_IN_YEAR;
    console.log("monthlypay "+monthlyPay);
    return(monthlyPay);
}

// Calculate annual pay using hours and pay rate
function getAnnualPay(hours,pay) {
    let annualPay = (hours * pay) * WEEKS_IN_YEAR;
    console.log("annualPay "+annualPay);
    return(annualPay);
}

// Display pay results on screen
function displayResults(weeklyPay,monthlyPay,annualPay) {
  document.getElementById("weeklyPay").innerHTML=weeklyPay.toFixed(2);
  document.getElementById("monthlyPay").innerHTML=monthlyPay.toFixed(2);
  document.getElementById("annualPay").innerHTML=annualPay.toFixed(2);
}









