// Maryann Jordan Web 200 Assignment 2 Activity 1
// months, days, hours, and seconds
const MONTHS_IN_YEAR=12;
const DAYS_IN_YEAR=365;
const HOURS_IN_YEAR=24*DAYS_IN_YEAR;
const SECONDS_IN_YEAR=60*HOURS_IN_YEAR;

"use strict";

// Prompt user for the age
let age=window.prompt("Enter Age in years :");
document.getElementById("age").innerHTML=age;


// call main function with inputted hours and pay rate
main(age);

// main function initiates other functions to calculate and display results to webpage
function main(age) {
    // Calculate age
    let months=getMonths(age);
    let days=getDays(age);
    let hours=getHours(age);
    let seconds=getSeconds(age);

    // Display Results on webpage
    displayResults(months,days,hours,seconds);
}

function getMonths(age) {
    let months=age*MONTHS_IN_YEAR;
    return(months);
}

function getDays(age) {
    let days=age*DAYS_IN_YEAR;
    return(days);
}

function getHours(age) {
    let hours=age*HOURS_IN_YEAR;
    return(hours);
}

function getSeconds(age) {
    let seconds=age*SECONDS_IN_YEAR;
    return(seconds);
}

// Display results on screen
function displayResults(months,days,hours,seconds) {
    document.getElementById("months").innerHTML=months.toFixed(2);
    document.getElementById("days").innerHTML=days.toFixed(2);
    document.getElementById("hours").innerHTML=hours.toFixed(2);
    document.getElementById("seconds").innerHTML=seconds.toFixed(2);
}

// Debug
console.log(`Input Age: `, age);
console.log("Months Old: ", months);
console.log("Days Old: ", days);
console.log("Hours Old: ", hours);
console.log("Seconds Old: ", seconds);

