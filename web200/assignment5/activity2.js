// Maryann Jordan Web 200 Assignment 5 Conditionals Activity 1
// months, days, hours, and seconds
const MONTHS_IN_YEAR=12;
const DAYS_IN_YEAR=365;
const HOURS_IN_YEAR=24*DAYS_IN_YEAR;
const SECONDS_IN_YEAR=60*HOURS_IN_YEAR;

"use strict";


function inputAge() {
    age = document.getElementById("inputAge").value;
    document.getElementById("age").innerHTML = age;
    console.log("input age ",age);
    return(age);
}

function getMonths(age) {
    let months=age*MONTHS_IN_YEAR;
    return(months);
}

function getDays(age) {
    let days=age*DAYS_IN_YEAR;
    return(days);
}

function getHours(age) {
    let hours=age*HOURS_IN_YEAR;
    return(hours);
}

function getSeconds(age) {
    let seconds=age*SECONDS_IN_YEAR;
    return(seconds);
}

function monthChecked() {
 
    console.log("checked month ",document.getElementById("checkMonth").checked);
    if ( document.getElementById("checkMonth").checked ) 
    {
        let months=getMonths(inputAge());
        document.getElementById("months").innerHTML=months.toFixed(2);
        return(true);
    }
    else
    {
        return(false);
    }
}

function daysChecked() {
   
    console.log("checked day ",document.getElementById("checkDays").checked);
    if ( document.getElementById("checkDays").checked ) 
    {
        let days=getDays(inputAge());
        document.getElementById("days").innerHTML=days.toFixed(2);
        return(true);
    }
    else
    {
        return(false);
    }
}

function hoursChecked() {

    console.log("checked hours ",document.getElementById("checkHours").checked);

    if ( document.getElementById("checkHours").checked ) 
    {
        let hours=getHours(inputAge());
        document.getElementById("hours").innerHTML=hours.toFixed(2);
        return(true);
    }
    else
    {
        return(false);
    }
}

function secondsChecked() {

    console.log("checked seconds ",document.getElementById("checkSeconds").checked);

    if ( document.getElementById("checkSeconds").checked ) 
    {
        let seconds=getSeconds(inputAge());
        document.getElementById("seconds").innerHTML=seconds.toFixed(2);
        return(true);
    }
    else
    {
        return(false);
    }
}

