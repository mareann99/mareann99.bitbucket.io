// Maryann Jordan Web 200

// Calculate weekly, monthly, and annual gross pay
// using inputted hours worked per week and rate per hour
const WEEKS_IN_YEAR=52;
const MONTHS_IN_YEAR=12;

"use strict";

// clear values
document.getElementById("inputPay").value="";
document.getElementById("inputHours").value="";

// wait for click on button
document.getElementById("myBtn").addEventListener("click", main);


function inputPayFn() {
    let hourlyPay = document.getElementById("inputPay").value;
    console.log("input pay ",hourlyPay);
    return(hourlyPay);
}

function inputHoursFn() {
    let hours = document.getElementById("inputHours").value;
    console.log("input hours ",hours);
    return(hours);
}


// main function initiates other functions to calculate and display results to webpage
function main() {
    let pay = inputPayFn();
    let hours = inputHoursFn(); 
    let weeklyPay = getWeeklyPay(hours,pay);  
    let monthlyPay = getMonthlyPay(hours,pay);
    let annualPay = getAnnualPay(hours,pay);
    console.log("main weeklyPay ",weeklyPay);
    // Display results to webpage
    document.getElementById("pay").innerHTML=pay;
    document.getElementById("hours").innerHTML=hours;
    displayResults(weeklyPay,monthlyPay,annualPay);
    // clear values

    document.getElementById("inputPay").value="";
    document.getElementById("inputHours").value="";
}


// Calculate weekly pay using hours and pay rate
function getWeeklyPay(hours,pay) {
   let weeklyPay = hours * pay;
   let overtimeRate = pay*1.5;
   if (hours > 40 ) 
      {
        weeklyPay = 40 * pay;
        console.log("getweek: before overtime weeklypay ",weeklyPay);
        weeklyPay = weeklyPay + ((hours-40) * overtimeRate); 
        console.log("overtime hours ",(hours-40));
      }
    console.log("getweek: hours ",hours);
    console.log("getweek: overtime rate ",overtimeRate);
    
    console.log("getweek: weeklypay ",weeklyPay);
    return(weeklyPay);
}

// Calculate monthly pay using hours and pay rate
function getMonthlyPay(hours,pay) {
    let monthlyPay = ((hours * pay) *  WEEKS_IN_YEAR) / MONTHS_IN_YEAR;
    let overtimeRate = pay*1.5;
    if (hours > 40)
      {
        monthlyPay = ((40 * pay) *  WEEKS_IN_YEAR) / MONTHS_IN_YEAR;
        console.log("monthlypay 40 hours ",monthlyPay);
        let overtimeMonth = ((hours-40) * overtimeRate *  WEEKS_IN_YEAR) / MONTHS_IN_YEAR;
        console.log("overtimeMonth ",overtimeMonth);
        monthlyPay = monthlyPay +  overtimeMonth;
      }
    console.log("monthlypay ",monthlyPay);
    return(monthlyPay);
}

// Calculate annual pay using hours and pay rate
function getAnnualPay(hours,pay) {
    let annualPay = (hours * pay) * WEEKS_IN_YEAR;
    let overtimeRate = pay*1.5;
    if (hours > 40)
      {
        annualPay = (40 * pay) * WEEKS_IN_YEAR;
        console.log("annualPay ",annualPay);
        let overtimeMonth = ((hours-40) * overtimeRate) * WEEKS_IN_YEAR;
        console.log("annualPay overtimeMonth ",overtimeMonth)
        annualPay += overtimeMonth;
        console.log("annualPay with overtime ",annualPay);       
      }
    console.log("annualPay ",annualPay);
    return(annualPay);
}

// Display pay results on screen
function displayResults(weeklyPay,monthlyPay,annualPay) {
  document.getElementById("weeklyPay").innerHTML=weeklyPay.toFixed(2);
  document.getElementById("monthlyPay").innerHTML=monthlyPay.toFixed(2);
  document.getElementById("annualPay").innerHTML=annualPay.toFixed(2);
  console.log("display annualPay ",annualPay);
  return;
}









