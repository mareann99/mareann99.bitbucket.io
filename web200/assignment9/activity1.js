// Maryann Jordan Web 200
// Display current date and time with separate outputs

"use strict";

// update display time every second
var myVar = setInterval(dateDisplay, 1000);

dateDisplay();

/*
    Add 0 padding if needed
    ex. if number n is 1, return "01"
*/
function nFormat(n){
    var ret = n > 9 ? "" + n: "0" + n;
    return (ret);
}

/*
    Display date MM/DD/YYYY
    Display time HH:mm:SS am/pm
*/
function dateDisplay() {
    var d = new Date();
    var amOrPm = (d.getHours() < 12) ? "AM" : "PM";
    var hour = (d.getHours() < 12) ? d.getHours() : d.getHours() - 12;
    // D A T E
    document.getElementById("curDay").innerHTML = nFormat(d.getDate()); //day as a number 1-31
    document.getElementById("curMonth").innerHTML = nFormat(d.getMonth() + 1); //as a number (0-11)
    document.getElementById("curYear").innerHTML = d.getFullYear(); //year as a four digit number (yyyy)
    document.getElementById("curDay").innerHTML = d.getDate(); //day as a number (1-31)

    // T I M E
    var sec = nFormat(d.getSeconds()) + " " + amOrPm;
    document.getElementById("curHour").innerHTML = nFormat(hour); //hour (0-23)
    document.getElementById("curMin").innerHTML = nFormat(d.getMinutes());  //minute (0-59)
    document.getElementById("curSec").innerHTML = sec; //second (0-59)

  }
