// Maryann Jordan Web 200
// Display date picker date with separate outputs

"use strict";

/*
    Display date MM/DD/YYYY
*/
function dateDisplay() {

    var x = document.getElementById("selectDate").value;

    var msec = Date.parse(x);
    var d = new Date(msec);
    d.setMinutes(d.getMinutes() + d.getTimezoneOffset());

    // D A T E
    document.getElementById("curMonth").innerHTML = nFormat(d.getMonth() + 1); //as a number (0-11)
    document.getElementById("curYear").innerHTML = d.getFullYear(); //year as a four digit number (yyyy)
    document.getElementById("curDay").innerHTML = nFormat(d.getDate()); //day as a number (01-31)
    
    //console.log("date change ",x);
}

/*
    Add 0 padding if needed
    ex. if number n is 1, return "01"
*/
function nFormat(n){
    var ret = n > 9 ? "" + n: "0" + n;
    return (ret);
}

/*
    Display date MM/DD/YYYY
    Display time HH:mm:SS
*/
function xdateDisplay() {
    var d = new Date();

    // D A T E
    document.getElementById("curDay").innerHTML = d.getDate(); //day as a number 1-31
    document.getElementById("curMonth").innerHTML = nFormat(d.getMonth() + 1); //as a number (0-11)
    document.getElementById("curYear").innerHTML = d.getFullYear(); //year as a four digit number (yyyy)
    document.getElementById("curDay").innerHTML = d.getDate(); //day as a number (1-31)

    // T I M E
    document.getElementById("curHour").innerHTML = nFormat(d.getHours()); //hour (0-23)
    document.getElementById("curMin").innerHTML = nFormat(d.getMinutes());  //minute (0-59)
    document.getElementById("curSec").innerHTML = nFormat(d.getSeconds()); //second (0-59)

  }
