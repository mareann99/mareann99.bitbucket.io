
    // Maryann Jordan Web 200 Assignment 2 Activity 2
    //Create a program that initializes different variables with integer, floating point, string, null, and undefined values
    
    var myInt = 22;
    var myString = "Flowers";
    // demo1 When adding a string to a number, the number is converted to a string and then added.
    var xResult = myInt + myString;
    document.getElementById("demo1").innerHTML ="<b>When adding a string to a number, the number is converted to a string and then added.</b><br><br>"+
    " "+"typeof("+myInt+") is " + typeof(myInt) +
    " PLUS "+"typeof("+myString+") is " + typeof(myString) +
    " RESULT "+"typeof("+xResult+") is " + typeof(xResult) +"<br> "+
    myInt + " + " + myString + " = " + xResult;
    console.log("int + string xResult ",xResult);
    
    // demo2 When adding a string to the sum of 2 numbers, the sum of the numbers is a number that is converted to a string and then added.
    var myInt2 = 11;
    var yResult = myInt + myInt2 + myString;
    document.getElementById("demo2").innerHTML = "<b>When adding a string to the sum of 2 numbers, the sum of the numbers is a number that is converted to a string and then added.(processed from left to right)</b><br><br>"+
    " "+"typeof("+myInt+") " + typeof(myInt) + " PLUS "+" typeof("+myInt2+") " + typeof(myInt2) +
    " PLUS "+"typeof("+myString+") " + typeof(myString) +
    " RESULT "+"typeof("+yResult+") " + typeof(yResult) +"<br> "+
    myInt + " + " + myInt2 + " + " + myString + " = "+yResult;

    // demo3 When adding a number and a float number to a string, the numbers are each converted to its own string and then added.
    var myFloat = 33.3;
    var zResult = myString + myInt + myFloat;  
    console.log("string + int + float zResult",zResult);
    document.getElementById("demo3").innerHTML = "<b>When adding a number and a float number to a string, the numbers are each converted to its own string and then added.(processed from left to right)</b><br><br>"+
    " "+"typeof("+myString+") " + typeof(myString) + " PLUS "+" typeof("+myInt+") " + typeof(myInt) +
    " PLUS "+"typeof("+myFloat+") " + typeof(myFloat) +
    " RESULT "+"typeof("+zResult+") " + typeof(zResult) +"<br> "+
    myInt + " + " + myInt2 + " + " + myString + " = "+yResult;

    var myNull = null;
    var myUndefined;
    var nResult = myInt + myNull;
    console.log("int + null nResult is ",nResult);
    document.getElementById("demo4").innerHTML = "<b>Adding a null value to an number, result is a number Or adding a number to a null value yields a number</b><br>"+
    " "+"typeof("+myInt+") " + typeof(myInt) + " PLUS "+" typeof("+myNull+") " + typeof(myNull) +
    " RESULT "+"typeof("+nResult+") " + typeof(nResult) +"<br> " +
    myInt  + " + " + myNull + " = "+nResult+"<br>"+myNull  + " + " + myInt + " = "+nResult+"<br>";
    nResult = myNull + myInt;
    console.log("null + int nResult is ",nResult);
    nResult = myFloat + myNull;
    console.log("float + null nResult is ",nResult);
    document.getElementById("demo4").innerHTML += "<br><b>Adding a undefined value to a number, result is a number</b><br>"+
    " "+"typeof("+myFloat+") " + typeof(myFloat) + " PLUS "+" typeof("+myUndefined+") " + typeof(myUndefined) +
    " RESULT "+"typeof("+nResult+") " + typeof(nResult) +
    "<br> " + myFloat  + " + " + myUndefined + " = "+nResult +
    "<br> " + myUndefined  + " + " + myFloat + " = "+nResult+"<br>";
    nResult = myInt + myUndefined;
    console.log("int + undefined nResult is ",nResult);
    nResult = myUndefined + myFloat;
    console.log("undefined + float nResult is ",nResult);

    nResult = myUndefined + myString;
    console.log("undefined nResult is ",nResult);
    var nResult2 = myString + myUndefined;
    console.log("undefined nResult2 is ",nResult2);
    document.getElementById("demo4").innerHTML += "<br><b>Adding a string to an undefined value, result is a string like undefinedStringValue </b><br>"+
    " "+"typeof("+myUndefined+") " + typeof(myUndefined) + " PLUS "+" typeof("+myString+") " + typeof(myString) +
    " RESULT "+"typeof("+nResult+") " + typeof(nResult) +
    "<br> " + myUndefined  + " + " + myString + " = "+nResult +
    "<br> " + myString  + " + " + myUndefined + " = "+nResult2+"<br>";




