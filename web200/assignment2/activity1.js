// Maryann Jordan Web 200 Assignment 2 Activity 1
// months, days, hours, and seconds
const MONTHS_IN_YEAR=12;
const DAYS_IN_YEAR=365;
const HOURS_IN_YEAR=24*DAYS_IN_YEAR;
const SECONDS_IN_YEAR=60*HOURS_IN_YEAR;

let age=window.prompt("Enter Age to convert:");

document.getElementById("age").innerHTML=age;

let months=age*MONTHS_IN_YEAR;
document.getElementById("months").innerHTML=months;

let days=age*DAYS_IN_YEAR;
document.getElementById("days").innerHTML=days;

let hours=age*HOURS_IN_YEAR;
document.getElementById("hours").innerHTML=hours;

let seconds=age*SECONDS_IN_YEAR;
document.getElementById("seconds").innerHTML=seconds;

console.log(`Input Age: `, age);
console.log("Months Old: ", months);
console.log("Days Old: ", days);
console.log("Hours Old: ", hours);
console.log("Seconds Old: ", seconds);

