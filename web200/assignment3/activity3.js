// Maryann Jordan Web 200 Assignment 3 Activity 3
// calculate and display the distance in yards, feet, and inches
// 1 mile = 1,760 yards = 5,280 feet = 63,360 inches
const FEET_PER_MILE=5280;
const YARDS_PER_MILE=1760;
const INCHES_PER_MILE=63360;

"use strict";

// prompt user for miles value to convert
let miles=window.prompt("Enter number of miles to convert");
document.getElementById("miles").innerHTML=miles;

// call main function with inputted miles value
main(miles);

// main function - calculate and display conversions
function main(miles) {
    let inches=miles*INCHES_PER_MILE;
    document.getElementById("inches").innerHTML=inches.toFixed(2);

    let feet=miles*FEET_PER_MILE;
    document.getElementById("feet").innerHTML=feet.toFixed(2);

    let yards=miles*YARDS_PER_MILE;
    document.getElementById("yards").innerHTML=yards.toFixed(2);
}