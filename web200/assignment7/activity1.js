// Maryann Jordan Web 200
// Calculate average of inputted grades

"use strict";

  var count = 0
  var sumOfGrades = 0;
  var currentGrade = 0;

  count = prompt("Enter Number of Grades : ");
  if ( count > 0 ) {
    var numberOfGrades = count; 
    document.getElementById("inputCount").innerHTML = count;

    for (var i=0; i< count; i++) 
    {
      currentGrade = prompt("Enter Grade : ");
      sumOfGrades += parseInt(currentGrade);
    }
    document.getElementById("sumOfGrades").innerHTML = sumOfGrades;
    document.getElementById("averageOfGrades").innerHTML = parseInt(sumOfGrades / numberOfGrades);
  }
