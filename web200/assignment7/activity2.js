// Maryann Jordan Web 200
// Create a program that uses a loop to generate a list of multiplication expressions for a given value. 
// Ask the user to enter the value and the number of expressions to be displayed. 

"use strict";

  var value = 0
  var sumOfGrades = 0;
  var currentGrade = 0;
  var numOfExpressions = 0;
  var expression = "";

  value = prompt("Enter value to multiply : ");
  numOfExpressions = prompt("Enter number of expressions : ");
  document.getElementById("numberValue").innerHTML = value;
  document.getElementById("inputExpressions").innerHTML = numOfExpressions;
  
  if ( numOfExpressions > 0 ) 
  {
    for (var i=1; i<= numOfExpressions; i++) 
    {
        expression = "";
        expression = value+" * "+i+" = "+(value*i)+"<br>";
        //alert(expression);
        document.getElementById("expressions").innerHTML += expression ;
    }
  }
