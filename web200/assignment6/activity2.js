// Maryann Jordan Web 200
// Guess number between 0 and 100

"use strict";

var answer = "N";
var low = 0;
var high = 100;
var middle = 50;

alert("Think about a number between 1 and 100");

do {

  document.getElementById("myGuess").innerHTML = middle;
  answer = prompt("Is "+middle+" your number? (higher or lower or yes) : ");
  middle = parseInt((high + low)/2); 

  if (answer == "lower" || answer == "l")
    {
      high = middle;
      middle= parseInt((high + low)/2);
    }
  else // higher
    {      
      low = middle;    
      middle= parseInt((high + low)/2);   
    }

} while ( answer == "higher" || answer == "lower" || answer == "h" || answer == "l") 

