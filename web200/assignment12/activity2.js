// Maryann Jordan Web 200
// Display book information
// Assignment 12 - Dynamic HTML

"use strict";
// APA format:[2]
// Last, F. M. (Year) Title. City, State: Publisher.

// object for book information
var BookListing = {
    firstName:"",
    lastName:"",
    pubYear:"",
    title:"",
    city:"",
    state:"",
    publisher:"",
    bookInfo : function() {
        // compose book info string for screen display
        var book = this.lastName;
        if (this.firstName)
            book += ", " + this.firstName;
        if (this.pubYear)
            book += " (" + this.pubYear +") ";
        if (this.title)
            book += this.title + ". ";
        if (this.city)
            book += this.city;
        if (this.state && this.city)
            book += ", ";
        if (this.state)
            book += this.state;
        if (this.publisher)
            book += ": " + this.publisher + ".";            
    
        return(book);
    }
}
      
// array of BookListing objects with book information
var bookList = [];

// wait for click on add book button
document.getElementById("myBtn").addEventListener("click", main);

/////////////////////////////////////////////////
//  field values
//  1     2       3     4     5       6     7
// Last, F. M. (Year) Title. City, State: Publisher.
// inputBookInfo
////////////////////////////////////////////////
function inputBookInfo(field) {
    var ret = 0;

    switch(field){
        case 1: 
            ret = document.getElementById("inputLastName").value;
            break;
        case 2: 
            ret = document.getElementById("inputFirstName").value;
            break;
        case 3: 
            ret = document.getElementById("inputPubYear").value;
            break;
        case 4: 
            ret = document.getElementById("inputTitle").value;
            break;
        case 5: 
            ret = document.getElementById("inputCity").value;
            break;
        case 6:
            ret = document.getElementById("inputState").value;
            break;
        case 7:
            ret = document.getElementById("inputPub").value;
            break;
       default: console.log("inputBookInfo invalid value ",field);
            break;
   }

   return(ret);
}

// main function gathers input information to display results to webpage
function main() {

    var b = BookListing;

    // fill object with input book information
    b.lastName = inputBookInfo(1);
    b.firstName = inputBookInfo(2);
    b.pubYear = inputBookInfo(3);
    b.title = inputBookInfo(4);
    b.city = inputBookInfo(5);
    b.state = inputBookInfo(6);
    b.publisher = inputBookInfo(7);
    
    if ( b.lastName != "" && (parseInt(b.pubYear) < 2999 && parseInt(b.pubYear) > 0) && b.title)
        bookList.push(b);
    else
     {
         alert("Please add valid book information before selecting add button");
         return;
     }

    // Display results to webpage
    var myBook = b.bookInfo(); /* + "<br>";*/
    document.getElementById("bookAdded").innerHTML = "You added " + myBook;

    let newLi = document.createElement('li');  // create new list item
    var node = document.createTextNode(myBook); // create text of book
    newLi.appendChild(node); // append text to list item
    var element = document.getElementById("dynamicBookList");
    element.appendChild(newLi); // append list item to dynamicBookList

    // clear values
    document.getElementById("inputLastName").value="";
    document.getElementById("inputFirstName").value="";
    document.getElementById("inputPubYear").value="";
    document.getElementById("inputTitle").value="";
    document.getElementById("inputCity").value="";
    document.getElementById("inputState").value="";
    document.getElementById("inputPub").value="";

} // end of main
