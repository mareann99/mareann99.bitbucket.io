// Maryann Jordan Web 200
// Display information 
// Final Assignment - Pizza App
// 
// Create a functional pizza order application using HTML, JavaScript, AJAX, and JSON.
// Include fields for name, address, and phone number.
// Allow for multiple toppings per pizza, with at least six toppings to choose from.
// Allow for multiple pizzas per order.
// Include support for at least three pizza sizes, with different prices for toppings based on pizza size.
// Dynamically display order information and price total as pizzas and toppings are added.
// Include 10% sales tax.
// Include a comments section for special notes and/or delivery instructions.
// Your solution will include an Order object, which contains a Customer object, an array of Pizza objects, and a nested array of toppings for each pizza.
// Use AJAX and JSON to submit order information.

"use strict";

// ref: https://en.wikiversity.org/wiki/JavaScript_Programming/AJAX_and_JSON/Example_Code
//testing data
function User(
    name=null, 
    username=null, 
    email=null, 
    address=null, 
    phone=null, 
    website=null,
    company=null) {
    this.name = name;
    this.username = username;
    this.email = email;
    this.address = address;
    this.phone = phone;
    this.website = website;
    this.company = company;
  }
  
  function Address(street=null, suite=null, city=null, zipcode=null, geolocation=null) {
    this.street = street;
    this.suite = suite;
    this.city = city;
    this.zipcode = zipcode;
    this.geo = geolocation;
  }
  
  function Geolocation(latitude=null, longitude=null) {
    this.lat = latitude;
    this.lng = longitude;
  }
  
  function Company(name=null, catchPhrase=null, bs=null) {
    this.name = name;
    this.catchPhrase = catchPhrase;
    this.bs = bs;
  }
  
window.addEventListener("load", function () {
  
 //   document.getElementById("get").addEventListener("click", getClick);
 //   document.getElementById("post").addEventListener("click", postClick);

    // listen for form control changes as they happen immediately
    document.getElementById("pizzaform").addEventListener("input", updateform);

//fixx    document.getElementById("get").focus();
  });

  document.getElementById("mySubmit").hidden = true;

  function createUser() {
    let user = new User(
      "John Smith",
      "jsmith", 
      "jsmith@example.com",
      null, 
      "123-555-1234");
  
    user.address = new Address(
      "123 Any Street", 
      "Suite 1", 
      "Anytown", 
      "12345");
  
    user.address.geo = new Geolocation(
      -40.7831, 
      73.9712);
  
    user.company = new Company(
      "Dewey, Cheatem & Howe",
      "Your loss is our gain",
      "law office")
  
    return user;
  }

// // testing section get data function
function getClick() {
    document.getElementById("url").innerText = 
    "https://jsonplaceholder.typicode.com/users/1?Leanne Graham"; // works!
    document.getElementById("data").innerText = "";
    document.getElementById("response").innerText = "";
    console.log("getFocus");
    let url = document.getElementById("url").innerText;
    let request = new XMLHttpRequest();
    request.open("GET", url);
    request.onload = function() {
      document.getElementById("response").innerText = "status: " + request.status + "\n";
      document.getElementById("response").innerText += "responseText:\n" + request.responseText;
      console.log("getClick() response ",document.getElementById("response").innerText);
      document.getElementById("data").innerText = request.responseText; // put in data section
    }
    request.send(null);
}

// // testing section post data function
function postClick() {
    document.getElementById("url").innerText = 
    "https://jsonplaceholder.typicode.com/users";

   // let user = createUser();
    let customer = createCustomerListing();
    document.getElementById("data").innerText = JSON.stringify(customer, null, 2);//= JSON.stringify(user, null, 2);
    document.getElementById("response").innerText = "";
    console.log("postFocus");
    let url = document.getElementById("url").innerText;
    let data = document.getElementById("data").innerText;
    let request = new XMLHttpRequest();
    request.open("POST", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = function() {
      document.getElementById("response").innerText = "status: " + request.status + "\n";
      document.getElementById("response").innerText += "responseText:\n" + request.responseText;
      //console.log("postClick() response ",document.getElementById("response").innerText);
    }
    request.send(data);
}

document.getElementById("url").innerText = "";
document.getElementById("data").innerText = "";
document.getElementById("response").innerText = "";

//////////////////////////////////////////////////////////////////////////////
// global
//////////////////////////////////////////////////////////////////////////////
var ingredientsList = [];
var submitFlag = 0;

// Last  First Address City  State Postal Email Phone
// object for Customer information
function CustomerListing(
  first,
  last,
  address,
  city,
  state,
  postal,
  email,
  phone) {
      this.firstName = first;
      this.lastName = last;
      this.address = address;
      this.city = city;
      this.state = state;
      this.postalCode = postal;
      this.emailAddress = email;
      this.phoneNUmber = phone;
  }

  function createCustomerListing() {
    let customer = new CustomerListing(
      "Joe",
      "Jones",
      "133 street",
      "chicago",
      "IL",
      "60646",
      "jj@example.com",
      "123-555-1234");
      return customer;
   }

  function pizzaObj  (
    size,
    crust,
    price,
    ingredientsList)
    {
     this.size = size;
     this.crust = crust;
     this.price = price;
     this.ingredientsList = ingredientsList;
  }
  
  var p = new pizzaObj();

  var OrderList = [{
    Customer: {
    Pizza:[],
    Info: new CustomerListing()
    }
  }];
  
  var d = OrderList[0];
  
  var pizzaCount = 0;
  var pizzaSize = "";
  var pizzaCrust = "";
  var toppingPizza = [];

  var pizzaPriceDetails = [
    [14.0, 1.00],
    [16.0, 1.50],
    [20.0, 2.00]
   ];

// array of CustomerListing objects with Customer information
var CustomerList = [];

var pizzaDetailsText = "";

clearInputFields();
document.getElementById("addOrder").disabled = true;

//createData();
function clearData() {
 // d.Customer.Info.firstName = "";
 // d.Customer.Info.lastName = "";
  // for (var i=0;i<d.Customer.Pizza.length;i++)
  //   d.Customer.Pizza = [];
  //d = [];
  //d.Customer.Pizza = [];
}

//create test data
function createData() {

    // create initial data 
    var c = new CustomerListing();

    // fill object with input information
    c.lastName     = "Smith";
    c.firstName    = "Mike";
    c.address      = "123 Main";
    c.postalCode   = "54321";
    CustomerList.push(c);
    if (CustomerList.length)
       console.log("create data length ",CustomerList.length,CustomerList[CustomerList.length-1]);
    else
       console.log("create error"); 
}  // end createData

function createInfo(field) {
  var ret = 0;
  switch(field){
      case 1: 
          document.getElementById("inputLastName").value = "Jones";
          break;
      case 2: 
          document.getElementById("inputFirstName").value = "Ray";
          break;
      case 3: 
          ret = document.getElementById("inputAddress").value;
          break;
      case 4: 
          ret = document.getElementById("inputCity").value;
          break;
      case 5:
          ret = document.getElementById("inputState").value;
          break;
      case 6:
          ret = document.getElementById("inputPostal").value;
          break;
      case 7:
          ret = document.getElementById("inputEmail").value;
          break;
      case 8:
          ret = document.getElementById("inputPhone").value;
          break;
      default: console.log("inputInfo invalid value ",field);
          break;
 }

 return(ret);
} // end createinfo

// wait for click on add to order Customer button
document.getElementById("addOrder").addEventListener("click", main);

document.getElementById("mySubmit").addEventListener("click", submitOrder);

document.getElementById("commentText").innerText = "";

//The POST method sends data to the server and creates a new resource.
function postData() {
  // initialize html fields
  document.getElementById("url").innerText = "";
  document.getElementById("data").innerText = "";
  document.getElementById("response").innerText = "";
  //if (OrderList.length) // if there is a customer, retrieve last customer
  document.getElementById("data").innerText = JSON.stringify(OrderList[0]);
  console.log("postData data ",document.getElementById("data").innerText);
  // Create a new object for storing the data.
  // Convert the new object to a string using your JSON parser.
  // Send the JSON string back to the client as the body.
  let data = document.getElementById("data").innerText;

  document.getElementById("response").innerText = "";

  let url = "https://mareann99.bitbucket.io/web200/final_project/activity1.html";
  //document.getElementById("url").innerText = url;

  let request = new XMLHttpRequest();
  request.open("POST", url, true);
  request.setRequestHeader("Content-Type", "application/json");
  
  // readystate codes
  //   0 (uninitialized) or (request not initialized)
  //   1 (loading) or (server connection established)
  //   2 (loaded) or (request received)
  //   3 (interactive) or (processing request)
  //   4 (complete) or (request finished and response is ready)

  request.onreadystatechange = function() {
    document.getElementById("response").innerText = "status: " + request.status + "\n";
    // document.getElementById("response").innerText += "responseText:\n" + request.responseText;
    //console.log("postData() response ",request.status);
  }
  console.log("postData() data ",data);

  // Send JSON Data from the Server Side
  request.send(data);

  clearData();

} // end postData

//////////////////////////////////////////////////////////////////////////////
//  field values
//  1     2       3     4     5      6      7      8       9    10
// Last  First Address City  State Postal Country Email Phone Date of Birth
// inputInfo
//////////////////////////////////////////////////////////////////////////////
function inputInfo(field) {
    var ret = 0;

    switch(field){
        case 1: 
            ret = document.getElementById("inputLastName").value;
            break;
        case 2: 
            ret = document.getElementById("inputFirstName").value;
            break;
        case 3: 
            ret = document.getElementById("inputAddress").value;
            break;
        case 4: 
            ret = document.getElementById("inputCity").value;
            break;
        case 5:
            ret = document.getElementById("inputState").value;
            break;
        case 6:
            ret = document.getElementById("inputPostal").value;
            break;
        case 7:
            ret = document.getElementById("inputEmail").value;
            break;
        case 8:
            ret = document.getElementById("inputPhone").value;
            break;
        default: console.log("inputInfo invalid value ",field);
            break;
   }

   return(ret);
}

//////////////////////////////////////////////////////////////////////////////
function clearInputFields() {
    // clear values
    document.getElementById("inputLastName").value="";
    document.getElementById("inputFirstName").value="";
    document.getElementById("inputAddress").value="";
    document.getElementById("inputCity").value="";
    document.getElementById("inputState").value="";
    document.getElementById("inputPostal").value="";
    document.getElementById("inputEmail").value="";
    document.getElementById("inputPhone").value="";

//console.log("clearInputFields customer info");
}

//////////////////////////////////////////////////////////////////////////////
function clearPizzaSelections() {
  document.getElementById("pizzaSize").selectedIndex = 0;
  document.getElementById("pizzaCrust").selectedIndex = 0;
  document.getElementById("pepperoni").checked = 0;
  document.getElementById("sausage").checked = 0;
  document.getElementById("tomatoes").checked = 0;
  document.getElementById("olives").checked = 0;
  document.getElementById("onions").checked = 0;
  document.getElementById("greenpeps").checked = 0;
  document.getElementById("totalItem").innerText = "0.00";
  document.getElementById("topPrice").innerHTML = "";

//console.log("clearPizzaSelections");
}

//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
function updateCustomerInfo() {

  d.Customer.Info.lastName     = inputInfo(1);
  d.Customer.Info.firstName    = inputInfo(2);  
  d.Customer.Info.address      = inputInfo(3);
  d.Customer.Info.city         = inputInfo(4);
  d.Customer.Info.state        = inputInfo(5);
  d.Customer.Info.postalCode   = inputInfo(6);
  d.Customer.Info.emailAddress = inputInfo(7);
  d.Customer.Info.phoneNumber  = inputInfo(8);

  //console.log("d.Customer.Info ",d.Customer.Info);

} // end updateCustomerInfo

//////////////////////////////////////////////////////////////////////////////
function getPrice(pizzaSizeIndex) {

  var pizzaPrice = 0.0;

  switch (pizzaSizeIndex) {
      case 1:
          pizzaPrice = pizzaPriceDetails[0][0];
          break;
      case 2:
          pizzaPrice = pizzaPriceDetails[1][0];
          break;
      case 3:
          pizzaPrice = pizzaPriceDetails[2][0];
          break;
      default:
          pizzaPrice = 0.0;
          break;
  }

  var ret = pizzaPrice;
  return (ret);
} // end getPrice

//////////////////////////////////////////////////////////////////////////////
function getTopPrice(pizzaSizeIndex) {
   var topPrice = 0.0;
 
   switch (pizzaSizeIndex) {
       case 1:
           topPrice = pizzaPriceDetails[0][1];
           break;
       case 2:
           topPrice = pizzaPriceDetails[1][1];
           break;
       case 3:
           topPrice = pizzaPriceDetails[2][1];
           break;
       default:
           topPrice = 0.0;
           break;
   }

   var ret = topPrice;
   return (ret);
 }

//////////////////////////////////////////////////////////////////////////////
 function getSizeText(pizzaSizeIndex) {
   var pizzaSize = "";
 
   switch (pizzaSizeIndex) {
       case 1:
           pizzaSize = "Small ";
           break;
       case 2:
           pizzaSize = "Medium ";
           break;
       case 3:
           pizzaSize = "Large ";
           break;
       default:
           console.log("unknown pizza size ",pizzaSizeIndex);
           break;
   }

   var ret = pizzaSize;
   return (ret);
 }

//////////////////////////////////////////////////////////////////////////////
 function getSizeText(pizzaSizeIndex) {
  var pizzaSize = "";

  switch (pizzaSizeIndex) {
      case 1:
          pizzaSize = "Small ";
          break;
      case 2:
          pizzaSize = "Medium ";
          break;
      case 3:
          pizzaSize = "Large ";
          break;
      default:
          console.log("unknown pizza size ",pizzaSizeIndex);
          break;
  }

  var ret = pizzaSize;
  return (ret);
}

//////////////////////////////////////////////////////////////////////////////
function getToppingText(pizzaToppingIndex) {
  var pizzaText = "";

  switch (pizzaToppingIndex) {
      case 1:
          pizzaText = "Pepperoni";
          break;
      case 2:
          pizzaText = "Sausage";
          break;
      case 3:
          pizzaText = "Tomatoes";
          break;
      case 4:
            pizzaText = "Olives";
            break;
      case 5:
            pizzaText = "Onions";
            break;
      case 6:
            pizzaText = "Green Peppers";
            break;
      default:
          console.log("unknown topping ",pizzaToppingIndex);
          break;
  }

  var ret = pizzaText;
  return (ret);
}

//////////////////////////////////////////////////////////////////////////////
function submitOrder() {

  alert("Thank you for your order!\n");

  //submitFlag=1;
  postData(); //send data using ajax
  // clear values
  document.getElementById("topPrice").innerHTML = "";
  document.getElementById("commentText").value = "";  
  document.getElementById("pizzaDetails").value = "";
  document.getElementById("pizzaDetailsPrice").value = "";
  document.getElementById("total").value = ""; 

  document.getElementById("mySubmit").hidden = true;

  // clear values
  clearInputFields();

} // end submitOrder

//////////////////////////////////////////////////////////////////////////////
function updateform() {

  var topPrice = 0.0;
  var pizzaPrice = 0.0;
  let totalamt = 0.00;
  var p = pizzaObj;
  var c = new CustomerListing();

  d.Customer.Info = c;

  let pizzaCrustIndex = document.getElementById("pizzaCrust").selectedIndex;
  var pizzaSizeIndex= document.getElementById("pizzaSize").selectedIndex;

  switch (pizzaSizeIndex) {
      case 1:
          pizzaPrice = pizzaPriceDetails[0][0];
          topPrice = pizzaPriceDetails[0][1];
          break;
      case 2:
          pizzaPrice = pizzaPriceDetails[1][0];
          topPrice = pizzaPriceDetails[1][1];
          break;
      case 3:
          pizzaPrice = pizzaPriceDetails[2][0];
          topPrice = pizzaPriceDetails[2][1];
          break;
      default:
          pizzaPrice = 0.0;
          topPrice = 0.0;
          break;
  }

  document.getElementById("topPrice").innerHTML = topPrice.toFixed(2);
  document.getElementById("totalItem").innerHTML = pizzaPrice.toFixed(2);

  totalamt += pizzaPrice;
  //console.log("updateform: totalamt ",totalamt," pizzaPrice ",pizzaPrice);  

  var pizzaToppingIndex = 1;
  var toppingCount = 0;

  var tops = document.getElementById("toppingsList").getElementsByClassName("toppingItem");
  for (const topping of tops) {
      if (topping.checked) {
          // number of toppings
          toppingCount++;
          totalamt += topPrice;
      }
      pizzaToppingIndex++;
  }
  document.getElementById("totalItem").innerHTML = totalamt.toFixed(2);

  if (pizzaSizeIndex && pizzaCrustIndex) {
      document.getElementById("addOrder").disabled = false;
  }

var valid = true;
document.getElementById("errorMsg").innerHTML = "";

//updateCustomerInfo();

d.Customer.Info.lastName     = inputInfo(1);
d.Customer.Info.firstName    = inputInfo(2);  

if (d.Customer.Info.firstName) {
  var inpObj = document.getElementById("inputFirstName");
  if (!inpObj.checkValidity()) {
    valid = false;
    document.getElementById("errorMsg").innerHTML = "First Name " + inpObj.validationMessage;
    document.getElementById("addOrder").disabled = true;
  }
  else
  {
    document.getElementById("errorMsg").innerHTML = "";
  }
}

 if (d.Customer.Info.lastName) {
  inpObj = document.getElementById("inputLastName");
  if (!inpObj.checkValidity()) {
     valid = false;
     document.getElementById("errorMsg").innerHTML = "Last Name " + inpObj.validationMessage;
     document.getElementById("addOrder").disabled = true;
  }
  else
   {
    document.getElementById("errorMsg").innerHTML = "";
  }
 }

 if ( d.Customer.Info.lastName &  d.Customer.Info.firstName)
 {
   document.getElementById("addOrder").disabled = true;
   valid = false;
 }
  //inpObj = document.getElementById("inputEmail");
  //if (!inpObj.checkValidity()) {
     //valid = false;
     //document.getElementById("errorMsg").innerHTML = "Email " + inpObj.validationMessage;
  //}
  inpObj = document.getElementById("inputPhone");
  if (!inpObj.checkValidity()) {
    valid = false;
    document.getElementById("errorMsg").innerHTML = "Phone " + inpObj.validationMessage;
 }
  inpObj = document.getElementById("inputAddress");
  if (!inpObj.checkValidity()) {
      valid = false;
      document.getElementById("errorMsg").innerHTML = "Address " + inpObj.validationMessage;
  }
  inpObj = document.getElementById("inputCity");
  if (!inpObj.checkValidity()) {
        valid = false;
        document.getElementById("errorMsg").innerHTML = "City " + inpObj.validationMessage;
  } 
  inpObj = document.getElementById("inputState");
  if (!inpObj.checkValidity()) {
        valid = false;
        document.getElementById("errorMsg").innerHTML = "State Abbreviation is two letters. " + inpObj.validationMessage;
  }
  inpObj = document.getElementById("inputPostal");
  if (!inpObj.checkValidity()) {
      valid = false;
      document.getElementById("errorMsg").innerHTML = "Postal Code is a 5 digit number. " + inpObj.validationMessage;
  } 

} // end updateform

//////////////////////////////////////////////////////////////////////////////
function getToppingFormText( index ) {

  var tops = document.getElementById("toppingsList").getElementsByClassName("toppingItem");
  var pizzaToppingIndex = 1;
  var toppingText = "";
  var toppingCount = 0;
  var toppingList = [];
  for (const topping of tops) {

      if (topping.checked) {
          toppingCount++;

          if (toppingCount === 1 || toppingCount === 6) {
            toppingText += getToppingText(pizzaToppingIndex);
          }
          else if (toppingCount === 4)      
            toppingText += "<br>" + getToppingText(pizzaToppingIndex);
          else
            toppingText += ", " + getToppingText(pizzaToppingIndex); 
          
          toppingList[toppingCount-1] = getToppingText(pizzaToppingIndex);
          console.log("toppingText ",toppingText," toppingList ",toppingList);
      }
      pizzaToppingIndex++;
  }

  return(toppingText);

}
//////////////////////////////////////////////////////////////////////////////
// main function gathers input information to display results to webpage
//////////////////////////////////////////////////////////////////////////////
function main() {

    let totalItem = document.getElementById("totalItem");
    let subTotal = 0.00;
    let totalamt = 0.00;
    var topPrice = 0.0;
    var pizzaPrice = 0.0;
    var totalLast = 0.0;
    var pzObj = new pizzaObj;

    d.Customer.Pizza[pizzaCount]= pzObj;
updateCustomerInfo();
  let pizzaCrustIndex = document.getElementById("pizzaCrust").selectedIndex;

  switch (pizzaCrustIndex) {
    case 1: pizzaCrust = "Thin";
            break;
    case 2: pizzaCrust= "Hand Tossed";
            break;
  }

  document.getElementById("errorMsg").innerHTML = "";
  var pizzaSizeIndex= document.getElementById("pizzaSize").selectedIndex;
 
  pizzaPrice = getPrice(pizzaSizeIndex);
  if (pizzaPrice == 0.00 || pizzaCrustIndex == 0)
  {
    if (document.getElementById("commentText").length == 0)
    {
       document.getElementById("pizzaDetails").innerHTML += "Customer Order<br><br>Special Instructions: " + document.getElementById("commentText").value;
       return;
    }

    document.getElementById("errorMsg").innerHTML = "<strong>Please select pizza size and crust</strong>";   
    return;
  }
  document.getElementById("addOrder").disabled = true; 
  topPrice = getTopPrice(pizzaSizeIndex);
  pizzaSize = getSizeText(pizzaSizeIndex);

  var tops = document.getElementById("toppingsList").getElementsByClassName("toppingItem");
  var pizzaToppingIndex = 1;
  var toppingText = "";
  var toppingCount = 0;
  var toppingList = [];
  for (const topping of tops) {

       if (topping.checked) {
           toppingCount++;

           totalamt += topPrice;
          if (toppingCount === 1 )
          toppingText += getToppingText(pizzaToppingIndex);
          else if ( toppingCount === 6)
            toppingText += ", " + getToppingText(pizzaToppingIndex);
          else if (toppingCount === 4)      
            toppingText += ",<br>" + getToppingText(pizzaToppingIndex);
          else
            toppingText += ", " + getToppingText(pizzaToppingIndex); 
          
           toppingList[toppingCount-1] = getToppingText(pizzaToppingIndex);
       }
       pizzaToppingIndex++;
   } // for topping
  //console.log("main totalamt ",totalamt," topPrice ",topPrice, "toppingCount ",toppingCount,"toppingText ", toppingText);

  totalamt += pizzaPrice;
  totalItem.value = String(totalamt);
  pzObj.price = Number(totalItem.value).toFixed(2);
  pzObj.size = pizzaSize;
  pzObj.crust = pizzaCrust;

  var tmp = 0.0;
  for (var k=0; k < d.Customer.Pizza.length;k++)
      tmp += Number(d.Customer.Pizza[k].price);

  subTotal = tmp;
  document.getElementById("total").innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;Subtotal:&nbsp;$" + tmp.toFixed(2) + "<br>";

  document.getElementById("pizzaDetailsPrice").innerHTML ="<br>";
  toppingPizza[pizzaCount] = toppingText;

  d.Customer.Pizza[pizzaCount].ingredientsList = toppingList;

  pizzaCount++;

  // console.log("MAIN INC pizzaCount is ",pizzaCount);
  // console.log("MAIN d pizza ",d.Customer.Pizza);
  // console.log("MAIN d info ",d.Customer.Info);

    // show Submit Order Button if ordered at least 1 pizza
    document.getElementById("mySubmit").hidden = false;

    ///// Print to Order Form //////
    document.getElementById("pizzaDetails").innerHTML = "Customer Order<br><br>";
    document.getElementById("pizzaDetailsPrice").innerHTML ="<br><br>";
    var tmpTax = 0.0;
    tmpTax += subTotal * 0.10;

    // add tax
    document.getElementById("total").innerHTML +=  "&nbsp;&nbsp;&nbsp;&nbsp;Tax:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$" + tmpTax.toFixed(2) + "<br>"; 

    // total
    document.getElementById("total").innerHTML += "&nbsp;&nbsp;&nbsp;&nbsp;Total:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$" + (subTotal + tmpTax).toFixed(2) + "<br>";

   for (var i=0; i < pizzaCount; i++) {

     if ( i ) {
       switch(d.Customer.Pizza[i-1].ingredientsList.length) {
          case 0:
              document.getElementById("pizzaDetails").innerHTML += "<br>" + d.Customer.Pizza[i].size + "  " + d.Customer.Pizza[i].crust + " Pizza ";
              document.getElementById("pizzaDetails").innerHTML += "<br>" + toppingPizza[i];
              document.getElementById("pizzaDetailsPrice").innerHTML +=  ".....$" + d.Customer.Pizza[i].price + "<br><br>";
              break;        
          case 1:
          case 2:
          case 3:
              document.getElementById("pizzaDetails").innerHTML += "<br><br>" + d.Customer.Pizza[i].size + "  " + d.Customer.Pizza[i].crust + " Pizza ";
              document.getElementById("pizzaDetails").innerHTML += "<br>" + toppingPizza[i];
              document.getElementById("pizzaDetailsPrice").innerHTML +=  "<br>.....$" + d.Customer.Pizza[i].price + "<br><br>";
              break;
          case 4:
          case 5:
          case 6: 
              document.getElementById("pizzaDetails").innerHTML += "<br><br>" + d.Customer.Pizza[i].size + "  " + d.Customer.Pizza[i].crust + " Pizza ";
              document.getElementById("pizzaDetails").innerHTML += "<br>" + toppingPizza[i];
              document.getElementById("pizzaDetailsPrice").innerHTML +=  "<br><br>.....$" + d.Customer.Pizza[i].price + "<br><br>"; 
              break;
          default:
              break;
        }
      } // more than 1 pizza
      else {
        document.getElementById("pizzaDetails").innerHTML += "<br>" + d.Customer.Pizza[i].size + "  " + d.Customer.Pizza[i].crust + " Pizza ";
        document.getElementById("pizzaDetails").innerHTML += "<br>" + toppingPizza[i];
        document.getElementById("pizzaDetailsPrice").innerHTML +=  "<br>.....$" + d.Customer.Pizza[i].price + "<br><br>"; 
      }

  } // end for pizzaCount

  var comments = document.getElementById("commentText").value;
  if (comments) {
    document.getElementById("pizzaDetails").innerHTML += "<br><br>Special Instructions: " + comments;
    //console.log("comments ",comments);
  }

  clearPizzaSelections();

} // end main
