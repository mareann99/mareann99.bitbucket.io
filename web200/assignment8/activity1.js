// Maryann Jordan Web 200
// Calculate average of inputted grades

"use strict";

  var count = 0
  var sumOfGrades = 0;
  //var currentGrade = 0;
  var grades = [];
  var index = 0;

  count = prompt("Enter Number of Grades : ");
  if ( count > 0 ) {
    var numberOfGrades = count; 
    document.getElementById("inputCount").innerHTML = count;

    while (count--)
    {
      index = numberOfGrades-count;
      grades[index] = prompt("Enter Grade : ");
      console.log("grades ",grades[index]);
      sumOfGrades += parseInt(grades[index]);
    }
    document.getElementById("sumOfGrades").innerHTML = sumOfGrades;
    document.getElementById("averageOfGrades").innerHTML = parseInt(sumOfGrades / numberOfGrades);
  }
