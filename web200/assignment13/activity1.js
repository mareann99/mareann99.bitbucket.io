// Maryann Jordan Web 200
// Display information 
// Assignment 13 - Forms and Security

"use strict";
// Last  First Address City  State Postal Email Phone Date of Birth
// object for Customer information
function CustomerListing(
    first,
    last,
    address,
    city,
    state,
    postal,
    country,
    email,
    phone,
    dob) {
        this.firstName = first;
        this.lastName = last;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postalCode = postal;
        this.country = country;
        this.emailAddress = email;
        this.phoneNUmber = phone;
        this.dateOfBirth = dob;
    }

// array of CustomerListing objects with Customer information
var CustomerList = [];

clearInputFields();

// wait for click on add Customer button
document.getElementById("mySubmit").addEventListener("click", main);

/////////////////////////////////////////////////
//  field values
//  1     2       3     4     5      6      7      8       9    10
// Last  First Address City  State Postal Country Email Phone Date of Birth
// inputInfo
////////////////////////////////////////////////
function inputInfo(field) {
    var ret = 0;

    switch(field){
        case 1: 
            ret = document.getElementById("inputLastName").value;
            break;
        case 2: 
            ret = document.getElementById("inputFirstName").value;
            break;
        case 3: 
            ret = document.getElementById("inputAddress").value;
            break;
        case 4: 
            ret = document.getElementById("inputCity").value;
            break;
        case 5:
            ret = document.getElementById("inputState").value;
            break;
        case 6:
            ret = document.getElementById("inputPostal").value;
            break;
        case 7:
            ret = document.getElementById("inputCountry").value;
            break;
        case 8:
            ret = document.getElementById("inputEmail").value;
            break;
        case 9:
            ret = document.getElementById("inputPhone").value;
            break;
        case 10:
            ret = document.getElementById("inputDOB").value;
            break;
        default: console.log("inputInfo invalid value ",field);
            break;
   }

   return(ret);
}
function clearInputFields() {
    // clear values
    document.getElementById("inputLastName").value="";
    document.getElementById("inputFirstName").value="";
    document.getElementById("inputAddress").value="";
    document.getElementById("inputCity").value="";
    document.getElementById("inputState").value="";
    document.getElementById("inputPostal").value="";
    document.getElementById("inputCountry").value="";
    document.getElementById("inputEmail").value="";
    document.getElementById("inputPhone").value="";
    document.getElementById("inputDOB").value="";
}
// main function gathers input information to display results to webpage
function main() {

    var c = new CustomerListing();

    // fill object with input information
    c.lastName     = inputInfo(1);
    c.firstName    = inputInfo(2);
    c.address      = inputInfo(3);
    c.city         = inputInfo(4);
    c.state        = inputInfo(5);
    c.postalCode   = inputInfo(6);
    c.country      = inputInfo(7);
    c.emailAddress = inputInfo(8);
    c.phoneNumber  = inputInfo(9);
    c.dateOfBirth  = inputInfo(10);

    if ( c.lastName != "" && c.firstName != "" && c.address != "")
    {
        document.getElementById("mySubmit").disabled = false;
        document.getElementById("errorMsg").innerHTML = "";

        // Last  First Address City  State Postal Country Email Phone Date of Birth
        document.getElementById("LastName").innerHTML = "You added " + c.lastName;
        
        if (c.firstName)
         document.getElementById("FirstName").innerHTML = ", " + c.firstName;

        if (c.address)
          document.getElementById("Address").innerHTML = ", " + c.address; 
        
        if (c.city)
            document.getElementById("City").innerHTML =  c.city;
        else
            document.getElementById("City").innerHTML =  "";
       
        if (c.state)
            document.getElementById("State").innerHTML = ", " + c.state;
        else
            document.getElementById("State").innerHTML = " ";
        
        if (c.postalCode)
            document.getElementById("PostalCode").innerHTML = ", " + c.postalCode;
        else
            document.getElementById("PostalCode").innerHTML = " ";

        if (c.emailAddress)
            document.getElementById("Email").innerHTML = " " + c.emailAddress;
        else
            document.getElementById("Email").innerHTML = " ";

        if (c.country)
            document.getElementById("Email").innerHTML = " " + c.country;
        else
            document.getElementById("Email").innerHTML = "USA";

        if (c.phoneNumber)
            document.getElementById("Phone").innerHTML = " " + c.phoneNumber;
        else
            document.getElementById("Phone").innerHTML = " ";

        if (c.dateOfBirth)
            document.getElementById("DateOfBirth").innerHTML = " " + c.dateOfBirth ;
        else
            document.getElementById("DateOfBirth").innerHTML = " ";

        CustomerList.push(c);
     }
    else
     {
        document.getElementById("errorMsg").innerHTML = "<strong>Please add valid Customer information before selecting add button</strong>";  
        // alert("Please add valid Customer information before selecting add button");
        return;
     }

    // Display results to webpage
    // Last  First Address City  State Postal Country Email Phone Date of Birth
    // compose Customer info string for screen display
    var myCustomer = c.lastName;
    if (c.firstName)
        myCustomer += ", " + c.firstName;
    if (c.address)
        myCustomer +=  ", " + c.address;
    if (c.city)
        myCustomer +=  ", " + c.city;
    if (c.state)
        myCustomer += ", " + c.state;
    if (c.postalCode)
        myCustomer +=  ", " + c.postalCode;
    if (c.country)
        myCustomer +=  ", " + c.country;
    if (c.emailAddress)
        myCustomer +=  ", " +   c.emailAddress;
    if (c.phoneNumber)
        myCustomer +=  ", " + c.phoneNumber;   
    if (c.dateOfBirth)
        myCustomer +=  ", " + c.dateOfBirth;

    let newLi = document.createElement('li');  // create new list item
    var node = document.createTextNode(myCustomer); // create text of Customer
    newLi.appendChild(node); // append text to list item
    var elementList = document.getElementById("dynamicCustomerList");
    elementList.appendChild(newLi); // append list item to dynamicCustomerList
    if (CustomerList.length == 1) // create Customer List Header
    {
        let newHeader3 = document.createElement("h3");
        var nodeHeader3 = document.createTextNode("Customer List");
        newHeader3.appendChild(nodeHeader3);
        elementList.insertBefore(newHeader3, elementList.childNodes[0]);
    }
    // clear values
    clearInputFields();

    //alert("Successfully Added "+myCustomer);
}
