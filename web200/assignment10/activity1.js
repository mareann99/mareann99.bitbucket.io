// Maryann Jordan Web 200
// Display book information using objects

"use strict";
// APA format:[2]
// Last, F. M. (Year) Title. City, State: Publisher.
document.getElementById("bookList").innerHTML = "";

// object for book information
function BookListing(
    first,
    last,
    year,
    title,
    city,
    state,
    publisher) {
        this.firstName = first;
        this.lastName = last;
        this.pubYear = year;
        this.title = title;
        this.city = city;
        this.state = state;
        this.publisher = publisher;
    }

// array of BookListing objects with book information
var bookList = [];

// wait for click on add book button
document.getElementById("myBtn").addEventListener("click", main);

/////////////////////////////////////////////////
//  field values
//  1     2       3     4     5       6     7
// Last, F. M. (Year) Title. City, State: Publisher.
// inputBookInfo
////////////////////////////////////////////////
function inputBookInfo(field) {
    var ret = 0;

    switch(field){
        case 1: 
            ret = document.getElementById("inputLastName").value;
            break;
        case 2: 
            ret = document.getElementById("inputFirstName").value;
            break;
        case 3: 
            ret = document.getElementById("inputPubYear").value;
            break;
        case 4: 
            ret = document.getElementById("inputTitle").value;
            break;
        case 5: 
            ret = document.getElementById("inputCity").value;
            break;
        case 6:
            ret = document.getElementById("inputState").value;
            break;
        case 7:
            ret = document.getElementById("inputPub").value;
            break;
        default: console.log("inputBookInfo invalid value ",field);
            break;
   }

   return(ret);
}

// main function gathers input information to display results to webpage
function main() {

    var b = new BookListing();

    // fill object with input book information
    b.lastName = inputBookInfo(1);
    b.firstName = inputBookInfo(2);
    b.pubYear = inputBookInfo(3);
    b.title = inputBookInfo(4);
    b.city = inputBookInfo(5);
    b.state = inputBookInfo(6);
    b.publisher = inputBookInfo(7);

    if ( b.lastName != "" && (parseInt(b.pubYear) < 2999 && parseInt(b.pubYear) > 0) && b.title)
    {
        // Last, F. M. (Year) Title. City, State: Publisher.
        document.getElementById("AuthorLastName").innerHTML = "You added " + b.lastName;
        
        if (b.firstName)
         document.getElementById("AuthorFirstName").innerHTML = ", " + b.firstName;

        if (b.pubYear)
          document.getElementById("PubYear").innerHTML = " (" + b.pubYear +") ";  

        if (b.title)
            document.getElementById("BookTitle").innerHTML = b.title + ". ";
        else
            document.getElementById("BookTitle").innerHTML = "";
        
        if (b.city)
            document.getElementById("City").innerHTML =  b.city;
        else
            document.getElementById("City").innerHTML =  "";
       
        if (b.state)
            document.getElementById("State").innerHTML = ", " + b.state;
        else
            document.getElementById("State").innerHTML = " ";
        
        if (b.publisher)
            document.getElementById("Publisher").innerHTML = ": " + b.publisher + ".";
        else
            document.getElementById("Publisher").innerHTML = " ";

        bookList.push(b);
     }
    else
     {
        alert("Please add valid book information before selecting add button");
        return;
     }


    // Display results to webpage
    // Last, F. M. (Year) Title. City, State: Publisher.

    // compose book info string for screen display
    var myBook = b.lastName;
    if (b.firstName)
        myBook += ", " + b.firstName;
    if (b.pubYear)
        myBook += " (" + b.pubYear +") ";
    if (b.title)
        myBook += b.title + ". ";
    if (b.city)
        myBook += b.city;
    if (b.state)
        myBook += ", " + b.state;
    if (b.publisher)
        myBook += ": " + b.publisher + ".";
    myBook +=  "<br>";   
    document.getElementById("bookList").innerHTML += myBook;

    // clear values
    document.getElementById("inputLastName").value="";
    document.getElementById("inputFirstName").value="";
    document.getElementById("inputPubYear").value="";
    document.getElementById("inputTitle").value="";
    document.getElementById("inputCity").value="";
    document.getElementById("inputState").value="";
    document.getElementById("inputPub").value="";

    //alert("Successfully Added "+myBook);
}
