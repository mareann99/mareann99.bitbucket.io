// Maryann Jordan Web 200
// display the nodeName and nodeType for all nodes

"use strict";

window.addEventListener("load", function () {

   displayChildNodes(document, 0)
});

function displayChildNodes(node, level) {
  console.log(`${" ".repeat(level * 4)}${node.nodeName}`)
  for(let i = 0; i < node.childNodes.length; i++) {
    displayChildNodes(node.childNodes[i], level + 1);
    //console.log("displayChildNodes i ",i," level ",level,"node ",node.childNodes[i]);//mj
  }
}
