// Maryann Jordan Web 200
// display the current window size, screen size, and location information
"use strict";

window.addEventListener("load", function () {

   displayWindow();
   console.log("");

   displayWindowDim();
   
   displayScreen();
   console.log("")

   displayNavigator();
   console.log("");

   displayLocation();
});

function myFunction() {
  displayWindowDim();
}

function displayWindow() {
  console.log(`window.innerWidth: ${window.innerWidth}`);
  console.log(`window.innerHeight: ${window.innerHeight}`);
}

function displayWindowDim() {
  document.getElementById("pgWidth").innerHTML = `screen width: ${window.innerWidth}`;
  document.getElementById("pgHeight").innerHTML = ` screen height: ${window.innerHeight}`;
}

function displayScreen() {
  console.log(`screen.width: ${screen.width}`);
  console.log(`screen.height: ${screen.height}`);
}

function displayNavigator() {
  console.log(`navigator.platform: ${navigator.platform}`);
  console.log(`navigator.userAgent: ${navigator.userAgent}`);
}

function displayLocation() {
  console.log(`location.hostname: ${location.hostname}`);
  console.log(`location.href: ${location.href}`);
}