// Maryann Jordan Web 200
// display all tags in the current HTML document
"use strict";

window.addEventListener("load", function () {
  displayElements("*");
  console.log("");
});

function displayElements(tag) {
  let elements = document.getElementsByTagName(tag);
  for (let i = 0; i < elements.length; i++) {
    let element = elements[i];
    console.log(element.tagName); 
  }
}
